FROM openjdk:8-alpine

#ENV PROFILE 'h2'
#ENV localPort '8090'
#ENV JAVA_OPTION_PROFILE '-Dspring.profiles.active='
#ENV JAVA_OPTION_PORT '-Dserver.port='

ARG JARNAME=dumy.jar
ARG JARPATH=dumypath

#TODO i can not pass absolute path as $CI_PROJECT_DIR to the JARPATH --build-arg ??
COPY $JARPATH/$JARNAME /home/user/app.jar

WORKDIR /home/user

ARG APP_PORT=9090

RUN adduser -D -h /home/user user

#TODO: check the nobody user
USER user

EXPOSE $APP_PORT

CMD java -jar app.jar
# CMD ["/bin/sh"]