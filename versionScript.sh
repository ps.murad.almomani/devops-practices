#!/bin/bash

git_last_commit() {
  lastGitCommit=$(git log -1)
}
#we have to put the echo Var in "" to not allow wcho to parse it to single line, so make issues in commands processing
git_8digit_hash() {
  if [ -z "$CI" ]; then
    hash8Digits=$(echo "$lastGitCommit" | grep 'commit' | cut -d " " -f 2 | cut -b 1-8)
  else
    hash8Digits=$CI_COMMIT_SHORT_SHA
  fi
}

git_Commit_Date() {
  gitCommitDate=$(echo "$lastGitCommit" | grep "Date" | awk -F ' ' '{print $3" "$4" "$6}')
}

#the -d option in date command is for displaying time described by STRING as specified by the format
git_Date_Formatted() {
  formattedDate=$(date -d"$1 $2 $3" +%y%m%d)
}

git_Version_Formatted() {
  formattedVersion=$1"-"$2
#  formattedVersion=$1

}

#getting last commit
lastGitCommit=''
git_last_commit
#echo "git_last_commit: $lastGitCommit "

#getting the commit 8digit-hash
hash8Digits=''
git_8digit_hash
#echo "hash8Digits: $hash8Digits"

#getting date from last commit
gitCommitDate=''
git_Commit_Date
#echo "gitCommitDate: $gitCommitDate"

#gettin formatted date yyyymmdd
##note here that the date like Jul 10 2020 were send each part as
##variable $1 $2 $3, you may double quote $gitCommitDate to send it as single unit
formattedDate=''
git_Date_Formatted $gitCommitDate
#echo "formattedDate: $formattedDate"

#getting version number in yymmdd-commitSha format
formattedVersion=''
git_Version_Formatted $formattedDate $hash8Digits
#echo "formattedVersion: $formattedVersion"
echo "$formattedVersion"